<?php

namespace Modules\User\Entities\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use League\Fractal;
use Modules\User\Entities\User;


class UserTransform extends Fractal\TransformerAbstract
{
    public function transform(User $user){
        return [
            'id'           => (int) $user->id,
            'name'         => $user->name,
            'email'        => $user->email,
            'image'        => $user->image,
            'password'     => $user->password,
            'roles'        => $user->getRoleNames(),
            'permissions'  => $user->getPermissionsViaRoles()->toArray(),
            'created_at'   => $user->created_at,
            'updated_at'   => $user->updated_at
        ];
    }
}
