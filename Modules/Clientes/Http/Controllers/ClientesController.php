<?php

namespace Modules\Clientes\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Clientes\Entities\Cliente;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;
/**
 * @SWG\Get(
 *     path="/create",
 *     description="Return a user's first and last name",
 *     @SWG\Parameter(
 *         name="firstname",
 *         in="query",
 *         type="string",
 *         description="Your first name",
 *         required=true,
 *     ),
 *     @SWG\Parameter(
 *         name="lastname",
 *         in="query",
 *         type="string",
 *         description="Your last name",
 *         required=true,
 *     ),
 *     @SWG\Response(
 *         response=200,
 *         description="OK",
 *     ),
 *     @SWG\Response(
 *         response=422,
 *         description="Missing Data"
 *     )
 * )
 */
class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('clientes::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function createCliente(Request $request)
    {
        $cliente = $request->all();
        Cliente::create($cliente);

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $data = Cliente::all();

        $projectsFormatted = Fractal::create()
            ->collection($data)
            ->transformWith(\Modules\Clientes\Entities\Transformers\ClienteTransform::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $projectsFormatted
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('clientes::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
