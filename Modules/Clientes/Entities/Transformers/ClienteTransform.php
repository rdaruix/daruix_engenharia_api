<?php

namespace Modules\Clientes\Entities\Transformers;

use League\Fractal;
use Modules\Clientes\Entities\Cliente;

class ClienteTransform extends Fractal\TransformerAbstract
{
    public function transform(Cliente $cliente){
        return [
            'id'           => (int) $cliente->id,
            'name'         => $cliente->name,
            'created_at'   => $cliente->created_at,
            'updated_at'   => $cliente->updated_at
        ];
    }
}
