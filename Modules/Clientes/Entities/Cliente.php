<?php

namespace Modules\Clientes\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    protected $fillable = ['id', 'name'];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
