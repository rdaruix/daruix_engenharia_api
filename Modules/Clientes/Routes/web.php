<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'jwt.auth', 'prefix' => 'v1/clientes'], function(){
    Route::post('/addCliente', 'ClientesController@createCliente');
    Route::get('/getClientes', 'ClientesController@show');
});
