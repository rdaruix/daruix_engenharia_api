<?php

namespace Modules\SubCategorias\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategoria extends Model
{
    protected $fillable = ['id', 'name'];
    use SoftDeletes;
    protected $dates = ['deleted_at'];


    public function getCategorias(){
        return $this->belongsTo('Modules\Categorias\Entities\Categoria', 'categoria_id');
    }

}
