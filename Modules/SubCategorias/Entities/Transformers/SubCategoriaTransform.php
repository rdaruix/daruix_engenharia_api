<?php

namespace Modules\SubCategorias\Entities\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use League\Fractal;
use Modules\SubCategorias\Entities\SubCategoria;

class SubCategoriaTransform extends Fractal\TransformerAbstract {
    public function transform(SubCategoria $subCategoria){
        return [
            'id'           => (int) $subCategoria->id,
            'name'         => $subCategoria->name,
            'categoria'    => $subCategoria->getCategorias()->getEager()->first(),
            'created_at'   => $subCategoria->created_at,
            'updated_at'   => $subCategoria->updated_at
        ];
    }

    protected $availableIncludes = [
        'categoria'
    ];
}
