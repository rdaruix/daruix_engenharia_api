<?php

namespace Modules\Categorias\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Categorias\Entities\Categoria;
use Spatie\Fractalistic\Fractal;
use Spatie\Fractalistic\ArraySerializer as Serializer;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('categorias::index');
    }

    /**
     * @SWG\Get(
     *     path="/addCategoria",
     *     description="Return a user's first and last name",
     *     @SWG\Parameter(
     *         name="firstname",
     *         in="query",
     *         type="string",
     *         description="Your first name",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="lastname",
     *         in="query",
     *         type="string",
     *         description="Your last name",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function createCategoria(Request $request)
    {
        $categoria = $request->all();
        Categoria::create($categoria);

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.'
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        $data = Categoria::all();

        $projectsFormatted = Fractal::create()
            ->collection($data)
            ->transformWith(\Modules\Categorias\Entities\Transformers\CategoriaTransform::class)
            ->serializeWith(new Serializer());

        return response()->json([
            'status' => 200,
            'slug' => 'response-ok',
            'message' => 'Response ok.',
            'data' => $projectsFormatted
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('categorias::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
