<?php

namespace Modules\Categorias\Entities\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use League\Fractal;
use Modules\Categorias\Entities\Categoria;

class CategoriaTransform extends Fractal\TransformerAbstract
{
    public function transform(Categoria $categoria){
        return [
            'id'                => (int) $categoria->id,
            'name'              => $categoria->name,
            'created_at'        => $categoria->created_at,
            'updated_at'        => $categoria->updated_at
        ];
    }
}
