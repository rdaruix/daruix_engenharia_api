<?php

namespace Modules\Categorias\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Categoria extends Model
{
    protected $fillable = ['id', 'name'];
    use SoftDeletes;
    protected $dates = ['deleted_at'];
}
