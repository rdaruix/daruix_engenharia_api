<?php

namespace Modules\Projeto\Entities\Transformers;

use League\Fractal;
use Modules\Projeto\Entities\Projeto;

class ProjetoTransform extends Fractal\TransformerAbstract
{
    public function transform(Projeto $projeto){
        return [
            'id'           => (int) $projeto->id,
            'name'         => $projeto->name,
            'images'       => $projeto->images,
            'categoria'    => $projeto->getCategorias()->getEager()->first(),
            'cliente'      => $projeto->getClientes()->getEager()->first(),
            'created_at'   => $projeto->created_at,
            'updated_at'   => $projeto->updated_at
        ];
    }

    protected $availableIncludes = [
        'subcategoria', 'clientes'
    ];
}
