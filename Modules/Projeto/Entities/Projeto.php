<?php

namespace Modules\Projeto\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Projeto extends Model
{
    protected $fillable = ['id', 'name', 'images', 'cliente_id', 'subcategoria_id', 'categoria_id'];
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function getClientes(){
        return $this->belongsTo('Modules\Clientes\Entities\Cliente', 'cliente_id');
    }

    public function getCategorias(){
        return $this->belongsTo('Modules\Categorias\Entities\Categoria', 'categoria_id');
    }
}
