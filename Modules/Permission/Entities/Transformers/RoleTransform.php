<?php

namespace Modules\Permission\Entities\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use League\Fractal;
use Spatie\Permission\Models\Role;

class RoleTransform extends Fractal\TransformerAbstract
{
    public function transform(Role $role){
        return [
            'id'           => (int) $role->id,
            'name'         => $role->name
        ];
    }
}
